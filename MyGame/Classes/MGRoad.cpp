//
//  MGRoad.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "MGRoad.h"

MGRoad::MGRoad()
{
  init("小草石.png");
  setAnchorPoint(Point(0,0.6));
}

MGRoad::MGRoad(std::string filename)
{
  init(filename);
  setAnchorPoint(Point(0,0.6));
}

bool MGRoad::init(std::string filename)
{
  if(!Sprite::init())
    return false;
  
  initWithFile(filename);
  mon_ = NULL;
  return true;
}

bool MGRoad::testOnGround(Point point)
{
  if(point.x < getPositionX() || point.x > getPositionX() + getContentSize().width)
    return false;
  return true;
}

bool MGRoad::testMonsterCollision(Point point,bool IsInvincible)
{
  if(!mon_)
    return false;
  bool ret = mon_->testOnCollision(point);
  if(IsInvincible && ret)
  {
    mon_->removeFromParentAndCleanup(true);
    mon_ = NULL;
    return true;
  }
  return ret;
}

void MGRoad::addMonster()
{
  mon_ = new MGMonster();
  mon_->setAnchorPoint(Point(0,0));
  int width = getContentSize().width - mon_->getContentSize().width;
  int randX = rand() % width;
  mon_->setPositionX(randX);
  mon_->setPositionY(getContentSize().height*0.6);
  addChild(mon_);
  mon_->autorelease();
}
