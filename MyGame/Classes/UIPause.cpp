//
//  UIPause.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "UIPause.h"
#include "HelloWorldScene.h"

UIPause::UIPause()
{
  initWithFile("暂停框_03.png");
  auto returnItem = MenuItemImage::create(
                                         "暂停框按钮07.png",
                                         "暂停框按钮07.png",
                                         CC_CALLBACK_1(UIPause::onReturn, this));
  
  returnItem->setAnchorPoint(Point(1,0));
  returnItem->setPosition(Point(getContentSize().width,40));
  
  auto continueItem = MenuItemImage::create(
                                         "暂停框按钮07.png",
                                         "暂停框按钮07.png",
                                         CC_CALLBACK_1(UIPause::onContinue, this));
  
  continueItem->setAnchorPoint(Point(0,0));
  continueItem->setPosition(Point(0,40));
  
  auto menu = Menu::create(continueItem,returnItem,NULL);
  menu->setPosition(Point::ZERO);
  this->addChild(menu, 1);
  
  LabelTTF* label1 = LabelTTF::create("暂停中。。。", "Verdana", 30);
  label1->setPosition(Point(getContentSize().width/2,getContentSize().height*0.6));
  label1->setAnchorPoint(Point(0.5,0.5));
  label1->setColor(Color3B::BLACK);
  addChild(label1,4);
  
  LabelTTF* label2 = LabelTTF::create("继续", "Verdana", 30);
  label2->setPosition(Point(continueItem->getContentSize().width/2,continueItem->getContentSize().height/2+5));
  label2->setAnchorPoint(Point(0.5,0.5));
  label2->setColor(Color3B::BLACK);
  continueItem->addChild(label2,4);
  
  LabelTTF* label3 = LabelTTF::create("退出", "Verdana", 30);
  label3->setPosition(Point(returnItem->getContentSize().width/2,returnItem->getContentSize().height/2+5));
  label3->setAnchorPoint(Point(0.5,0.5));
  label3->setColor(Color3B::BLACK);
  returnItem->addChild(label3,4);

}

void UIPause::onReturn(Object* pSender)
{
  TransitionScene* transition = TransitionFadeBL::create(1.0f, HelloWorld::createScene());
  Director::getInstance()->replaceScene(transition);
  Director::getInstance()->resume();
}

void UIPause::onContinue(Object* pSender)
{
  Director::getInstance()->resume();
  removeFromParentAndCleanup(true);
}