//
//  UIGameOver.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__UIGameOver__
#define __MyGame__UIGameOver__

#include "cocos2d.h"
using namespace cocos2d;

class UIGameOver : public Sprite
{
public:
  UIGameOver(int score);
  void onReturn(Object* pSender);
};

#endif /* defined(__MyGame__UIGameOver__) */
