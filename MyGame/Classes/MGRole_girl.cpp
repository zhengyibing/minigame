//
//  MGRole_girl.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "MGRole_girl.h"
#include "CommonFunctions.h"

bool MGRole_girl::init()
{
  Sprite::init();
  Animation* anim = CommonFunctions::getAnimation("girl.plist", "girl.png", "girl");
  if(anim->getFrames().size() <= 0)
    return false;
  auto frames = anim->getFrames();
  setDisplayFrame(frames.at(0)->getSpriteFrame());
  setAnchorPoint(Point(1,0));
  return true;
}

void MGRole_girl::run()
{
  stop();
  setStatus(roleRun);
  setVisible(true);
  Animation* ani = CommonFunctions::getAnimation("girl.plist", "girl.png", "girl");
  runAction(RepeatForever::create(Animate::create(ani)));
}

void MGRole_girl::skill()
{
  if(getIsInvincible())
    return;
  setIsInvincible(true);
  stopAllActions();
  Animation* ani = CommonFunctions::getAnimation("attack.plist", "attack.png", "attack");
  runAction(Sequence::create(Animate::create(ani),
                             CallFunc::create(CC_CALLBACK_0(MGRole::run,this)),
                             NULL));
  
  Animation* ani2 = CommonFunctions::getAnimation("攻击特效.plist", "攻击特效.png", "texiao");
  if(ani2->getFrames().size() <= 0)
    return;
  auto frames = ani2->getFrames();
  Sprite* spEffect = Sprite::createWithSpriteFrame(frames.at(0)->getSpriteFrame());
  spEffect->setAnchorPoint(Point(0.5,0));
  spEffect->setPosition(Point(getContentSize().width,0));
  addChild(spEffect,-1);
  spEffect->runAction(Sequence::create(Animate::create(ani2),
                                       CallFuncN::create(CC_CALLBACK_1(MGRole_girl::skillEnd,this)),
                                       NULL));
}

void MGRole_girl::skillEnd(Node* node)
{
  node->removeFromParentAndCleanup(true);
  setIsInvincible(false);
}