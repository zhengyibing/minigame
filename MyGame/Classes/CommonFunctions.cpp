//
//  CommonFunctions.cpp
//  MyGame
//
//  Created by admin on 14-1-24.
//
//

#include "CommonFunctions.h"

Animation* CommonFunctions::getAnimation(string plist, string png, string prefix,float delay)
{
  SpriteFrameCache* cache = SpriteFrameCache::getInstance();
  
  cache->addSpriteFramesWithFile(plist, png);
  
  //创建动画每一帧，从cache里面读取
  Vector<SpriteFrame*> animFrames ;
  
  char str[64] = {0};
  int i = 10000;
  while (true) {
    sprintf(str, "%s %d",prefix.c_str() ,i);
    SpriteFrame* frame = cache->getSpriteFrameByName( str );
    if(!frame)
      break;
    animFrames.pushBack(frame);
    i++;
  }
  
  return Animation::createWithSpriteFrames(animFrames,delay);
  
  cache->removeSpriteFramesFromFile(plist);

  return NULL;
}