//
//  MGItem.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__MGItem__
#define __MyGame__MGItem__

#include "cocos2d.h"
using namespace cocos2d;

class MGItem : public Sprite
{
public:
  MGItem();
  bool init();
  bool testOnCollision(Point point);
};

#endif /* defined(__MyGame__MGItem__) */
