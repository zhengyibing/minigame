//
//  UIPause.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__UIPause__
#define __MyGame__UIPause__

#include "cocos2d.h"
using namespace cocos2d;

class UIPause : public Sprite
{
public:
  UIPause();
  void onReturn(Object* pSender);
  void onContinue(Object* pSender);
};

#endif /* defined(__MyGame__UIPause__) */
