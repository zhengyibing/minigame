//
//  CommonFunctions.h
//  MyGame
//
//  Created by admin on 14-1-24.
//
//

#ifndef __MyGame__CommonFunctions__
#define __MyGame__CommonFunctions__

#include "cocos2d.h"
#include <string.h>
using namespace cocos2d;
using namespace std;

class CommonFunctions
{
public:
  static Animation* getAnimation(string plist, string png, string prefix,float delay = 0.04f);
};

#endif /* defined(__MyGame__CommonFunctions__) */
