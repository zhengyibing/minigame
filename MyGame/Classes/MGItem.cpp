//
//  MGItem.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "MGItem.h"
MGItem::MGItem()
{
  init();
  setAnchorPoint(Point(0.5,0.6));
}

bool MGItem::init()
{
  if(!Sprite::init())
    return false;
  
  int rd = rand()%2;
  if(rd >1)
    initWithFile("云1.png");
  else
    initWithFile("云2.png");
  return true;
}

bool MGItem::testOnCollision(Point point)
{
  float des = point.getDistance(getPosition());
  if(des < 100)
    return true;
  return false;
}