//
//  GameScene.cpp
//  MyGame
//
//  Created by admin on 14-1-24.
//
//

#include "GameScene.h"
#include "CommonFunctions.h"
#include "MGRole_girl.h"
#include "GlobalParas.h"
#include "UIPause.h"
#include "UIGameOver.h"
Scene* GameScene::createScene()
{
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  
  // 'layer' is an autorelease object
  auto layer = GameScene::create();
  
  // add layer as a child to scene
  scene->addChild(layer);
  
  // return the scene
  return scene;
}

bool GameScene::init()
{
  if ( !Layer::init() )
  {
    return false;
  }
  roleA_ = NULL;
  roleB_ = NULL;
  speed_ = 256.0f;
  score_ = 0;
  level_ = 1;
  countDown_ = 5;
  status_ = gameWIN;
  initBg();
  initMenu();
  initRoad();
  return true;
}

void GameScene::onEnterTransitionDidFinish()
{
  Animation* anim =CommonFunctions::getAnimation("数字.plist", "数字.png", "su",1.0f);
  if(anim->getFrames().size() <= 0)
    return;
  auto frames = anim->getFrames();
  countDownSprite_ = Sprite::createWithSpriteFrame(frames.at(0)->getSpriteFrame());
  addChild(countDownSprite_,10);
  countDownSprite_->setPosition(Point(getContentSize().width/2,getContentSize().height*0.7));
  countDownSprite_->runAction(Sequence::create(Animate::create(anim),
                                               CallFunc::create(CC_CALLBACK_0(GameScene::gameStart,this)),
                                               NULL));

}

void GameScene::initBg()
{
  setContentSize(Director::getInstance()->getWinSize());
  auto bg = Sprite::create("主界面背景.png");
  bg->setPosition(Point(0,0));
  bg->setAnchorPoint(Point(0,0));
  float sx = Director::getInstance()->getWinSize().width / bg->getContentSize().width;
  float sy = Director::getInstance()->getWinSize().height / bg->getContentSize().height;
  bg->setScaleX(sx);
  bg->setScaleY(sy);
  addChild(bg,-1);
  
  Animation* anim = CommonFunctions::getAnimation("sun.plist", "sun.png", "sun");
  if(anim->getFrames().size() <= 0)
    return;
  auto frames = anim->getFrames();
  auto sun = Sprite::createWithSpriteFrame((frames.at(0))->getSpriteFrame());
  sun->setAnchorPoint(Point(1,1));
  sun->setPosition(Point(getContentSize().width,getContentSize().height));
  sun->runAction(RepeatForever::create(Animate::create(anim)));
  addChild(sun);
  
  LabelTTF* scoreLabel = LabelTTF::create("得分", "Verdana", 20);
  scoreLabel->setAnchorPoint(Point(0,1));
  scoreLabel->setPosition(Point(50,getContentSize().height-50));
  addChild(scoreLabel);
  
  auto score_bg = Sprite::create("得分墨迹_01.png");
  score_bg->setAnchorPoint(Point(0,1));
  score_bg->setPosition(Point(10,getContentSize().height-10));
  addChild(score_bg,-1);
  
  labelScore_ = LabelTTF::create("0", "Verdana", 20);
  labelScore_->setAnchorPoint(Point(0,1));
  labelScore_->setPosition(Point(Point(scoreLabel->getPositionX() + scoreLabel->getContentSize().width + 20,
                                       scoreLabel->getPositionY())));
  addChild(labelScore_);
  
  
}

void GameScene::initMenu()
{
  auto shiftItem = MenuItemImage::create(
                                         "交换人物按钮.png",
                                         "交换人物按钮.png",
                                         CC_CALLBACK_1(GameScene::shiftRole, this));
  shiftItem->setAnchorPoint(Point(0,0));
  shiftItem->setPosition(Point(0,0));
  
  auto skillItem = MenuItemImage::create(
                                         "特殊技能按钮.png",
                                         "特殊技能按钮.png",
                                         CC_CALLBACK_1(GameScene::playSkill, this));

  skillItem->setAnchorPoint(Point(1,0));
  skillItem->setPosition(Point(getContentSize().width,0));
  
  auto pauseItem = MenuItemImage::create(
                                         "暂停按钮.png",
                                         "暂停按钮.png",
                                         CC_CALLBACK_1(GameScene::onPause, this));
  
  pauseItem->setAnchorPoint(Point(1,1));
  pauseItem->setPosition(Point(getContentSize().width,getContentSize().height));
  
  auto menu = Menu::create(shiftItem,skillItem,pauseItem,NULL);
  menu->setPosition(Point::ZERO);
  this->addChild(menu, 1);
}

void GameScene::initRoad()
{
  addRoad(0);
  addItem(0);
}

void GameScene::initRoles()
{
  Size size = Director::getInstance()->getWinSize();
  roleA_ = new MGRole();
  roleA_->setPosition(Point(size.width/2,size.height/2));
  addChild(roleA_,1);
  roleA_->autorelease();
  
  roleB_ = new MGRole_girl();
  roleB_->setPosition(Point(size.width/2,size.height/2));
  addChild(roleB_,1);
  roleB_->autorelease();
  
  roleA_->run();
  runner_ = roleA_;
}

void GameScene::shiftRole(Object* pSender)
{
  if(!roleA_)
    return;
  RoleStatus a = roleA_->getStatus();
  RoleStatus b = roleB_->getStatus();
  roleA_->shiftRole(b);
  roleB_->shiftRole(a);
  if(roleA_->getStatus() == roleRun)
    runner_ = roleA_;
  else
    runner_ = roleB_;
}

void GameScene::playSkill(Object* pSender)
{
  if(runner_)
    runner_->skill();
}

void GameScene::onPause(Object* pSender)
{
  if(status_ == gameLose)
    return;
  if(Director::getInstance()->isPaused())
    return;
  else
  {
    Director::getInstance()->pause();
    UIPause* p = new UIPause();
    addChild(p,3);
    p->setPosition(Point(getContentSize().width/2,getContentSize().height/2));
    p->autorelease();
  }
}

void GameScene::testLand(float t)
{
  float des = runner_->getPositionY() - GlobalParas::getGround();
  if(des > 0)
    runner_->drop(GlobalParas::getGround());
  else
  {
    int i = 0;
    for(;i<roads_.size();i++)
    {
      if(roads_.at(i)->testOnGround(runner_->getPosition()) ||
         roads_.at(i)->testOnGround(Point(runner_->getPositionX()-runner_->getContentSize().width*0.7,
                                          runner_->getPositionY())))
        break;
    }
    if(i == roads_.size())
    {
      gameOver();
    }
  }
  
  for(int j = 0;j<roads_.size();j++)
  {
    if(roads_.at(j)->testMonsterCollision(Point(runner_->getPositionX()-runner_->getContentSize().width*0.7,
                                                runner_->getPositionY()),runner_->getIsInvincible()))
    {
      onCollisionWithMonster();
      break;
    }
  }
}

void GameScene::addRoad(float t)
{
  unschedule(schedule_selector(GameScene::addRoad));
  updateSpeed();
  MGRoad* tmp ;
  
  //简单的对生成道路的判断
  if(score_ < 1000)
    tmp = new MGRoad();
  else
    tmp = new MGRoad("青石.png");
  tmp->setPosition(Point(getContentSize().width,GlobalParas::getGround()));
  if(isAddMonster())
    tmp->addMonster();
  addChild(tmp);
  tmp->autorelease();
  roads_.pushBack(tmp);
  float des = getContentSize().width + tmp->getContentSize().width;
  float t_next = tmp->getContentSize().width / speed_;
  tmp->runAction(Sequence::create(MoveBy::create(des/speed_, Point( -des,0)),
                                  CallFuncN::create(CC_CALLBACK_1(GameScene::removeRoad,this)),
                                  NULL));
  float next = t_next ;
  if(level_ >1)
  next += ( rand() % (30+level_*5))/100.0f;
  schedule(schedule_selector(GameScene::addRoad), next);
}

void GameScene::addItem(float t)
{
  unschedule(schedule_selector(GameScene::addItem));
  updateSpeed();
  MGItem* tmp = new MGItem();
  int height = getContentSize().height;
  tmp->setPosition(Point(getContentSize().width,getContentSize().height*0.6 + rand() % height* 0.2));
  addChild(tmp);
  items_.pushBack(tmp);
  tmp->autorelease();
  float des = getContentSize().width + tmp->getContentSize().width;
  float t_next = 0.5f + (rand() % 300) /100.0f;
  tmp->runAction(Sequence::create(MoveBy::create(4.0f, Point( -des,0)),
                                  CallFuncN::create(CC_CALLBACK_1(GameScene::removeItem,this)),
                                  NULL));
  
  schedule(schedule_selector(GameScene::addItem), t_next);
}

void GameScene::removeItem(Node* node)
{
  MGItem* item = (MGItem*)node;
  items_.eraseObject(item);
  item->removeFromParentAndCleanup(true);
  
  score_ += 1;
  char buf[50] = {0};
  sprintf(buf, "%d", score_);
  labelScore_->setString(buf);
}

void GameScene::removeRoad(Node* node)
{
  MGRoad* road = (MGRoad*)node;
  roads_.eraseObject(road);
  road->removeFromParentAndCleanup(true);
  score_ += 10* (speed_ / 128.0);
  char buf[50] = {0};
  sprintf(buf, "%d", score_);
  labelScore_->setString(buf);
}

void GameScene::updateSpeed()
{
  if(score_ > level_*level_ * 100  && speed_ <2048 )
  {
    speed_ *= 1.1;
    level_ ++;
  }
  for(int i = 0;i<roads_.size();i++)
  {
    roads_.at(i)->stopAllActions();
  }
  for(int i = 0;i<roads_.size();i++)
  {
    float des = roads_.at(i)->getPositionX() + roads_.at(i)->getContentSize().width;
    roads_.at(i)->runAction(Sequence::create(MoveTo::create(des/speed_, Point(-roads_.at(i)->getContentSize().width,
                                                                              roads_.at(i)->getPositionY())),
                                             CallFuncN::create(CC_CALLBACK_1(GameScene::removeRoad,this)),
                                             NULL));
  }
}

void GameScene::onCollisionWithMonster()
{
  if(runner_->getIsInvincible())
  {
    score_ += 100;
    char buf[50] = {0};
    sprintf(buf, "%d", score_);
    labelScore_->setString(buf);
  }
  else
    gameOver();
}

void GameScene::gameOver()
{
  status_ = gameLose;
  unscheduleAllSelectors();
  Director::getInstance()->pause();
  UIGameOver* gv = new UIGameOver(score_);
  addChild(gv,3);
  gv->setPosition(Point(getContentSize().width/2,getContentSize().height/2));
  gv->autorelease();
}

void GameScene::gameStart()
{
  countDownSprite_->removeFromParentAndCleanup(true);
  initRoles();
  schedule(schedule_selector(GameScene::testLand),0.04f);
}

bool GameScene::isAddMonster()
{
  int f = (level_ * 20);
  if(f > rand() % 100)
    return true;
  return false;
}
