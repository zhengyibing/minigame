//
//  TutorialScene.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "TutorialScene.h"
#include "GameScene.h"
Scene* TutorialScene::createScene()
{
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  
  // 'layer' is an autorelease object
  auto layer = TutorialScene::create();  
  
  // add layer as a child to scene
  scene->addChild(layer);
  
  // return the scene
  return scene;
}

bool TutorialScene::init()
{
  if ( !Layer::init() )
  {
    return false;
  }
  setContentSize(Director::getInstance()->getWinSize());
  auto image = Sprite::create("新手介绍.png");
  image->setPosition(Point(0,0));
  image->setAnchorPoint(Point(0,0));
  image->setScaleX(Director::getInstance()->getWinSize().width / image->getContentSize().width);
  image->setScaleY(Director::getInstance()->getWinSize().height / image->getContentSize().height);
  addChild(image);
  
  auto beginItem = MenuItemImage::create(
                                         "关闭按钮.png",
                                         "关闭按钮.png",
                                         CC_CALLBACK_1(TutorialScene::menuCloseCallback, this));
  Size size = getContentSize();
  beginItem->setAnchorPoint(Point(1,1));
  beginItem->setPosition(Point(size.width,size.height));
  auto menu = Menu::create(beginItem, NULL);
  menu->setPosition(Point::ZERO);
  this->addChild(menu, 1);

  return true;
}

void TutorialScene::menuCloseCallback(Object* pSender)
{
  TransitionScene* transition = TransitionFadeTR::create(1.0f, GameScene::createScene());
  Director::getInstance()->replaceScene(transition);
}