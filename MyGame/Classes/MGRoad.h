//
//  MGRoad.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__MGRoad__
#define __MyGame__MGRoad__

#include "cocos2d.h"
#include "MGMonster.h"
#include <string.h>
using namespace cocos2d;

class MGRoad : public Sprite
{
public:
  MGRoad();
  MGRoad(std::string filename);
  bool init(std::string filename);
  bool testOnGround(Point point);
  bool testMonsterCollision(Point point,bool IsInvincible);
  void addMonster();
private:
  MGMonster* mon_;
};

#endif /* defined(__MyGame__MGRoad__) */
