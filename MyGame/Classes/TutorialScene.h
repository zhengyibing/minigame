//
//  TutorialScene.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__TutorialScene__
#define __MyGame__TutorialScene__

#include "cocos2d.h"

using namespace cocos2d;


class TutorialScene : public Layer
{
public:
  static cocos2d::Scene* createScene();
  virtual bool init();
  void menuCloseCallback(Object* pSender);
  CREATE_FUNC(TutorialScene);
};

#endif /* defined(__MyGame__TutorialScene__) */
