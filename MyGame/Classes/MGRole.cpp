//
//  MGRole.cpp
//  MyGame
//
//  Created by admin on 14-1-24.
//
//

#include "MGRole.h"
#include "CommonFunctions.h"
#include "GlobalParas.h"

MGRole::MGRole()
{
  alive = true;
  status = roleSleep;  
  init();
  setVisible(false);
  setAnchorPoint(Point(1,0));
  setIsInvincible(false);
  bJump = false;
}

MGRole::~MGRole()
{
}

Object* MGRole::get(string key)
{
  return dict_.at(key);
}

void  MGRole::del(string key)
{
  dict_.erase(key);
}

void MGRole::set(Object* value,string key)
{
  dict_.insert(key,value);
}

bool MGRole::init()
{  
  Sprite::init();
  Animation* anim = CommonFunctions::getAnimation("11.plist", "11.png", "Tween");
  if(anim->getFrames().size() <= 0)
    return false;
  auto frames = anim->getFrames();
  setDisplayFrame(frames.at(0)->getSpriteFrame());
  return true;
}

void MGRole::run()
{
  stop();
  setStatus(roleRun);
  setVisible(true);
  Animation* ani = CommonFunctions::getAnimation("11.plist", "11.png", "Tween");  
  runAction(RepeatForever::create(Animate::create(ani)));
}

void MGRole::sleep()
{
  stop();
  setStatus(roleSleep);
  setVisible(false);
}

void MGRole::stop()
{
  stopAllActions();
  setJump(false);
}

void MGRole::shiftRole(RoleStatus otherStatus)
{
  switch (status) {
    case roleRun:
    {
      switch (otherStatus)
      {
        case roleRun:
        case roleSleep:
        {
          sleep();
        }
          break;
        default:
          break;
      }
        
    }
      break;
    case roleSleep:
    {
      run();
    }
      break;
      
    default:
      break;
  }
};

void MGRole::win()
{
  stop();
}

void MGRole::drop(float ground)
{
  if(bJump) //如果正在跳起来或者正在降落，不降落
    return;
  bJump = true;
  float des = getPositionY() - ground;
  runAction(Sequence::create(MoveTo::create(des / 375.0f, Point(getPositionX(),ground)),
                             CallFunc::create(CC_CALLBACK_0(MGRole::run, this)),
                             NULL));
}

void MGRole::skill()
{
  if(getJump())
    return;
  setJump(true);
  stopAllActions();
  Animation* ani = CommonFunctions::getAnimation("tiao.plist", "tiao.png", "tiao");
  runAction(Animate::create(ani));
  
  runAction(Sequence::create(MoveTo::create(0.6f, Point(getPositionX(),getPositionY() + 150.0f)),
                             CallFunc::create(CC_CALLBACK_0(MGRole::jumpEnd, this)),
                             NULL));
}

void MGRole::jumpEnd()
{
  setJump(false);
}


