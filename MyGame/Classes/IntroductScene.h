//
//  IntroductScene.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__IntroductScene__
#define __MyGame__IntroductScene__

#include "cocos2d.h"

using namespace cocos2d;


class IntroductScene : public Layer
{
public:
  static cocos2d::Scene* createScene();
  virtual bool init();
  CREATE_FUNC(IntroductScene);
  
  void loadEnd(float t);
};

#endif /* defined(__MyGame__IntroductScene__) */
