//
//  IntroductScene.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "IntroductScene.h"
#include "HelloWorldScene.h"
#include "CommonFunctions.h"

Scene* IntroductScene::createScene()
{
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  
  // 'layer' is an autorelease object
  auto layer = IntroductScene::create();
  
  // add layer as a child to scene
  scene->addChild(layer);
  
  // return the scene
  return scene;
}

bool IntroductScene::init()
{
  if ( !Layer::init() )
  {
    return false;
  }
  setContentSize(Director::getInstance()->getWinSize());
  auto image = Sprite::create("加载背景.png");
  image->setPosition(Point(0,0));
  image->setAnchorPoint(Point(0,0));
  image->setScaleX(Director::getInstance()->getWinSize().width / image->getContentSize().width);
  image->setScaleY(Director::getInstance()->getWinSize().height / image->getContentSize().height);
  addChild(image);
  
  LabelTTF* label1 = LabelTTF::create("制作人员:杨帆 郑一冰", "Verdana", 70);
  label1->setPosition(Point(getContentSize().width/2,getContentSize().height * 0.8));
  label1->setAnchorPoint(Point(0.5,0.5));
  addChild(label1);
  LabelTTF* label2 = LabelTTF::create("少侠稍等片刻......", "Verdana", 25);
  label2->setPosition(Point(getContentSize().width/2,getContentSize().height * 0.5));
  label2->setAnchorPoint(Point(0.5,0.5));
  addChild(label2);
  LabelTTF* label3 = LabelTTF::create("Tips:妹子可以攻击怪物，汉子可以跳跃障碍物", "Verdana", 35);
  label3->setPosition(Point(getContentSize().width/2,getContentSize().height * 0.2));
  label3->setAnchorPoint(Point(0.5,0.5));
  addChild(label3);
  
  
  Animation* anim = CommonFunctions::getAnimation("girl.plist", "girl.png", "girl");
  if(anim->getFrames().size() <= 0)
    return false;
  auto frames = anim->getFrames();
  Sprite* sprite = Sprite::createWithSpriteFrame(frames.at(0)->getSpriteFrame());
  sprite->runAction(RepeatForever::create(Animate::create(anim)));
  sprite->setPosition(label2->getPosition() + Point(label2->getContentSize().width/2,15));
  sprite->setAnchorPoint(Point(0,0.5));
  sprite->setScale(label2->getContentSize().height/ sprite->getContentSize().height *1.8 );
  addChild(sprite, 5);
  scheduleOnce(schedule_selector(IntroductScene::loadEnd), 5.0f);
  return true;
}

void IntroductScene::loadEnd(float t)
{
  TransitionScene* transition = TransitionFadeTR::create(1.0f, HelloWorld::createScene());
  
  // run
  Director::getInstance()->replaceScene(transition);

}
