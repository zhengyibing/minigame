//
//  MGRole.h
//  MyGame
//
//  Created by admin on 14-1-24.
//
//

#ifndef __MyGame__mgRole__
#define __MyGame__mgRole__

#include "cocos2d.h"
#include <string.h>
using namespace std;
using namespace cocos2d;

enum RoleStatus
{
  roleDie = 0,  //角色死亡
  roleSleep ,    //没在场上
  roleRun,      //正在酷跑
};

class MGRole : public Sprite
{
  CC_SYNTHESIZE(bool, alive, IsAlive);    //该角色是否活着，暂时没用上
  CC_SYNTHESIZE(RoleStatus, status, Status);
  CC_SYNTHESIZE(bool, bJump, Jump);
  CC_SYNTHESIZE(bool, bInvincible, IsInvincible);
public:
  MGRole();
  ~MGRole();
  Object* get(string key);
  void del(string key);
  void set(Object* value,string key);
  virtual bool init();
  virtual void shiftRole(RoleStatus otherStatus);
  virtual void run();     //跑
  virtual void skill();   //技能
  virtual void sleep();   //休息
  virtual void stop();
  virtual void win();
  virtual void drop(float ground);
private:
  void jumpEnd();
  Map<string,Object*> dict_;
};

#endif /* defined(__MyGame__mgRole__) */
