//
//  MGRole_girl.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__MGRole_girl__
#define __MyGame__MGRole_girl__

#include "MGRole.h"
class MGRole_girl : public MGRole
{
public:
  virtual bool init();
  virtual void run();
  virtual void skill();
  void skillEnd(Node* node);
};
#endif /* defined(__MyGame__MGRole_girl__) */
