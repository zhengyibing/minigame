//
//  MGMonster.h
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#ifndef __MyGame__MGMonster__
#define __MyGame__MGMonster__

#include "cocos2d.h"
using namespace cocos2d;

class MGMonster : public Sprite
{
public:
  MGMonster();
  bool init();
  bool testOnCollision(Point point);
};

#endif /* defined(__MyGame__MGMonster__) */
