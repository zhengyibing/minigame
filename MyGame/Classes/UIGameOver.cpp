//
//  UIGameOver.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "UIGameOver.h"
#include "HelloWorldScene.h"


UIGameOver::UIGameOver(int score)
{
  initWithFile("暂停框_03.png");
  auto returnItem = MenuItemImage::create(
                                          "暂停框按钮07.png",
                                          "暂停框按钮07.png",
                                          CC_CALLBACK_1(UIGameOver::onReturn, this));
  
  returnItem->setAnchorPoint(Point(0.5,0.5));
  returnItem->setPosition(Point(getContentSize().width /2,getContentSize().height *0.3));

  
  auto menu = Menu::create(returnItem,NULL);
  menu->setPosition(Point::ZERO);
  this->addChild(menu, 1);
  
  char buf[50];
  sprintf(buf, "游戏结束\n得分:%d",score);
  LabelTTF* label1 = LabelTTF::create(buf, "Verdana", 30);
  label1->setHorizontalAlignment(TextHAlignment(1));
  label1->setPosition(Point(getContentSize().width/2,getContentSize().height*0.6));
  label1->setAnchorPoint(Point(0.5,0.5));
  label1->setColor(Color3B::BLACK);
  addChild(label1,4);

  
  LabelTTF* label3 = LabelTTF::create("退出", "Verdana", 30);
  label3->setPosition(Point(returnItem->getContentSize().width/2,returnItem->getContentSize().height/2+5));
  label3->setAnchorPoint(Point(0.5,0.5));
  label3->setColor(Color3B::BLACK);
  returnItem->addChild(label3,4);
  
}

void UIGameOver::onReturn(Object* pSender)
{  
  TransitionScene* transition = TransitionFadeBL::create(1.0f, HelloWorld::createScene());
  Director::getInstance()->replaceScene(transition);
  Director::getInstance()->resume();
}

