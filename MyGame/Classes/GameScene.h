//
//  GameScene.h
//  MyGame
//
//  Created by admin on 14-1-24.
//
//

#ifndef __MyGame__GameScene__
#define __MyGame__GameScene__

#include "cocos2d.h"
#include "MGRole.h"
#include "MGRoad.h"
#include "MGItem.h"
using namespace cocos2d;

enum GameStatus
{
  gameWIN = 0,
  gameLose ,
};

class GameScene : public Layer
{
public:  
  static cocos2d::Scene* createScene();
  
  virtual bool init();
  
  // a selector callback
  void menuCloseCallback(Object* pSender);
  void onEnterTransitionDidFinish();
  void update();
  CREATE_FUNC(GameScene);
private:
  int countDown_;
  int score_;
  int level_;
  float speed_ ;
  GameStatus status_;
  MGRole* roleA_;
  MGRole* roleB_;
  MGRole* runner_;
  LabelTTF*  labelScore_;
  Sprite* countDownSprite_;
  Vector<MGRoad*> roads_;      //路面
  Vector<MGItem*> items_;     //飘在空中的物件（云彩等），可以吃
  
private:
  void initBg();  //初始化背景
  void initMenu();//初始化各种按钮
  void initRoad();
  void initRoles();
  void removeRoad(Node* node);
  void removeItem(Node* node);
  void shiftRole(Object* pSender);
  void playSkill(Object* pSender);
  void onPause(Object* pSender);
  void testLand(float t);
  void addRoad(float t);
  void addItem(float t);
  void updateSpeed();
  void onCollisionWithMonster();
  void gameOver();
  void gameStart();
  bool isAddMonster();
};

#endif /* defined(__MyGame__GameScene__) */
