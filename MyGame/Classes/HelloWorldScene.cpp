#include "HelloWorldScene.h"
#include "CommonFunctions.h"
#include "TutorialScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
  //////////////////////////////
  // 1. super init first
  if ( !Layer::init() )
  {
      return false;
  }

  Size size = Director::getInstance()->getWinSize();
  
  auto bg = CCSprite::create("开始背景.png");
  bg->setPosition(Point(0,0));
  bg->setAnchorPoint(Point(0,0));
  float sx = Director::getInstance()->getWinSize().width / bg->getContentSize().width;
  float sy = Director::getInstance()->getWinSize().height / bg->getContentSize().height;
  bg->setScaleX(sx);
  bg->setScaleY(sy);
  addChild(bg,-1);

  auto beginItem = MenuItemImage::create(
                                         "开始按钮亮.png",
                                         "开始按钮按下.png",
                                         CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
  beginItem->setPosition(Point(size.width/2,size.height*0.25));
  auto menu = Menu::create(beginItem, NULL);
  menu->setPosition(Point::ZERO);
  this->addChild(menu, 1);


  auto sprite = Sprite::create("游戏名称.png");
  sprite->setPosition(Point(size.width/2, beginItem->getContentSize().height/2 +beginItem->getPosition().y + 150));
  this->addChild(sprite, 0);
  
  return true;
}

void HelloWorld::menuCloseCallback(Object* pSender)
{
  TransitionScene* transition = TransitionFadeTR::create(1.0f, TutorialScene::createScene());
  Director::getInstance()->replaceScene(transition);
//    Director::getInstance()->end();

//#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
//    exit(0);
//#endif
}
