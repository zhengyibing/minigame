//
//  MGMonster.cpp
//  MyGame
//
//  Created by admin on 14-1-25.
//
//

#include "MGMonster.h"
MGMonster::MGMonster()
{
  init();
  setAnchorPoint(Point(0.5,0.6));
}

bool MGMonster::init()
{
  if(!Sprite::init())
    return false;
  
  int rd = rand()%6;
  if(rd >4)
    initWithFile("BigMonster1_02.png");
  else if(rd > 3)
    initWithFile("BigMonster1_05.png");
  else if(rd > 2)
    initWithFile("BigMonster1_11.png");
  else
    initWithFile("地刺.png");
  return true;
}

bool MGMonster::testOnCollision(Point point)
{
  float des = point.getDistance(getParent()->convertToWorldSpace(getPosition()));
  if(des < 50)
    return true;
  return false;
}